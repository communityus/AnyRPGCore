# AnyRPG

## Introduction

This project contains the code for the AnyRPG Engine.

AnyRPG is a Role Playing Game engine written in C# for Unity.

It is a free, open source project with the goal of enabling storytellers to tell their stories in the format of a Role Playing Game quickly and easily.

It accomplishes this by providing a platform with the most common game functionality out of the box.

Content creators only need to provide visual assets and story content to create unique and compelling short stories, scenarios, adventures, and even full games.

## Web Site

http://www.anyrpg.org/

## YouTube Channel

https://www.youtube.com/channel/UC-SiqAyRXR6eijPggFhFG2g