﻿using UnityEngine;

public interface IMoveable{

    Sprite MyIcon { get; }
    string MyName { get; }
}