﻿
public enum CombatType { normal, ability, gainHealth, gainXP, levelUp, gainMana, gainBuff, loseBuff }

public enum CombatMagnitude { normal, critical }
