﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationProfileNode {

    public AnimationClip animationClip;

    public int numHits = 1;
}
