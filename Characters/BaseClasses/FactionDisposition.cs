﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FactionDisposition {

    public Faction faction;
    public string factionName;
    public float disposition;
}
