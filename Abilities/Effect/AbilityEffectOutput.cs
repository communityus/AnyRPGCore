﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityEffectOutput {

    public int healthAmount = 0;
    public int manaAmount = 0;

    public Vector3 prefabLocation = Vector3.zero;
}
